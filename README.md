# Nectar

A document writing and generation library for GNU Guile

## Usage

Convert Nectar to SXML

```scheme
(use-modules (nectar parser)
             (nectar evaluate))


(define tree (nectar->sxml (file->nectar "test.nct")))
```

Convert Nectar to HTML (with automatic paragraph breaking)

```scheme
(use-modules (nectar parser)
             (nectar evaluate)
             (nectar renderers html))

(define html-string (nectar-ast->html (nectar->nectar-ast (file->nectar "test.nct"))))
```

## Syntax

A nectar file `.nct` follows Racket's scribble syntax closely, with
slightly different whitespace and nesting semantics (lists are spliced)
to generalize to arbitrarily many output formats.

```
@(define variable "test")

This is a @variable item.

@(let loop ((i 0)
            (lst '()))
   (if (not (< i 10))
      (reverse lst)
      (loop (+ i 1)
            (cons @p{@|i|: These are some @variable items and @list{spliced text}!} lst))))

This is @b{bold text}.

@(define (extra-bold . items)
    @b[#:class "extra-bold"]{@items})

This is @extra-bold{EXTRA BOLD} text.
```

Converted to HTML becomes

```html
<p>This is a test item.</p>
<p>0: These are some test items and spliced text!</p>
<p>1: These are some test items and spliced text!</p>
<p>2: These are some test items and spliced text!</p>
<p>3: These are some test items and spliced text!</p>
<p>4: These are some test items and spliced text!</p>
<p>5: These are some test items and spliced text!</p>
<p>6: These are some test items and spliced text!</p>
<p>7: These are some test items and spliced text!</p>
<p>8: These are some test items and spliced text!</p>
<p>9: These are some test items and spliced text!</p>
<p>This is <b>bold text</b>.</p>
<p>This is <b class="extra-bold">EXTRA BOLD</b> text.</p>
```

### Renderer notes

The HTML renderer renders two elements specially to facilate precise
output requirements.

```
@passthrough{<br>}
```

The passthrough element takes the HTML provided and inserts it without processing into the output.

```
@inline{text @b{text}}
```

The inline element acts like an inline element and prevents paragraph
breaking of the text inside, however the text is HTML escaped and
rendered normally. This can be useful to `<li>` or `<td>` elements.
