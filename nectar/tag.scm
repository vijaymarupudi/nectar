(define-module (nectar tag)
               #:export (<tag>
                          tag-name
                          tag-attrs
                          tag-children
                          tag?
                          make-tag))

(use-modules (srfi srfi-9)
             (srfi srfi-9 gnu))

(define-record-type <tag>
  (make-tag name attrs children)
  tag?
  (name tag-name tag-name-set!)
  (attrs tag-attrs tag-attrs-set!)
  (children tag-children tag-children-set!))

(define-public (tag-with-children x children)
  (make-tag (tag-name x)
            (tag-attrs x)
            children))

(set-record-type-printer! <tag>
  (lambda (x port)
    (display "<" port)
    (display (tag-name x) port)
    (display " " port)
    (display "attrs=" port)
    (write (tag-attrs x) port)
    (display " " port)
    (display "children=" port)
    (write (tag-children x) port)
    (display ">" port)))
