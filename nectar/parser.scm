(define-module (nectar parser))

(use-modules (ice-9 textual-ports)
             (ice-9 match)
             (srfi srfi-9) ; records
             (system syntax)
             (srfi srfi-1) ; list
             (srfi srfi-42) ; eager comprehension
             (srfi srfi-11) ; let-values
             (nectar utils) 
             )

(define at-token-param (make-parameter "@"))
(define open-curly-token-param (make-parameter "{"))
(define close-curly-token-param (make-parameter "}"))

(define-record-type <token-tester>
  (%make-token-tester token detector tag)
  token-tester?
  (token token-tester-token)
  (detector token-tester-detector)
  (tag token-tester-tag))

(define (make-token-tester token tag)
  (%make-token-tester token (make-incremental-string-detector token) tag))

(define (token-tester-feed token-tester ch)
  ((token-tester-detector token-tester) ch))

(define (param->token-tester-lambda param tag)
  (lambda () (make-token-tester (param) tag)))

(define make-token-at-tester
  (param->token-tester-lambda at-token-param 'at))
(define make-token-open-curly-tester
  (param->token-tester-lambda open-curly-token-param 'open-curly))
(define make-token-close-curly-tester
  (param->token-tester-lambda close-curly-token-param 'close-curly))

(define-record-type <@expr>
  (make-@expr expr)
  @expr?
  (expr @expr-expr))

; (define-record-type <sexpr>
;   (make-sexpr expr)
;   sexpr?
;   (expr sexpr-expr))


(define (read-until port first-ch pred)
  (let loop ((chars (if first-ch
                      (list first-ch)
                      '())))
    (let ((ch (peek-char port)))
      (cond
        ((or (eof-object? ch)
             (pred ch))
         (reverse-list->string chars))
        (else
          (read-char port)
          (loop (cons ch chars)))))))

(define (read-token port ch)
  (read-until port ch char-delimiter?))


(define (read-delimiter port ch)
  (read-until port ch (lambda (ch)
                        (not (char-delimiter? ch)))))

(define (read-whitespace port ch)
  (read-until port ch (lambda (ch)
                        (not (char-whitespace? ch)))))

(define SPECIAL_INITIALS "!$%&*/:<=>?^_~")
(define SPECIAL_INITIALS_HASHMAP
  (let loop ((i 0)
        (h (make-hash-table (string-length SPECIAL_INITIALS))))
    (if (= i (string-length SPECIAL_INITIALS))
      h
      (begin
        (hashv-set! h (string-ref SPECIAL_INITIALS i) #t)
        (loop (+ i 1) h)))))

(define (char-special-initial? c)
  (hashv-ref SPECIAL_INITIALS_HASHMAP c))

(define (char-delimiter? ch)
  (case ch
    ((#\( #\) #\; #\" #\space #\return #\ff #\newline #\tab) #t)
    ; maybe insert @ here again?
    ((#\{ #\} #\[ #\]) #t)
    (else #f)))

(define (char-letter? c)
  (let ((i (char->integer c)))
    (or (and (<= i 122)
             (>= i 97))
        (and (<= i 90)
             (>= i 65)))))

(define (char-initial? c)
  (or (char-letter? c)
       (char-special-initial? c)))

(define (char-subsequent? c)
  (or (char-initial? c)
      (char-digit? c)
      (char-special-subsequent? c)))

(define (char-sign-subsequent? c)
  (case c
    ((#\- #\+ #\@) #t)
    (else (char-initial? c))))

(define (char-dot-subsequent? c)
  (or (char-sign-subsequent? c)
      (char=? c #\.)))

(define (char-special-subsequent? c)
  (string-index "+-.@" c))

(define (char-digit? c)
  (define i (char->integer c))
  (and (<= i 57)
       (>= i 48)))

; returns three values, the text, and the reason, and the token
(define (parse-free-guile-text port ch)
  (let ((testers (list (make-token-at-tester)
                       (make-token-open-curly-tester)
                       (make-token-close-curly-tester))))
    (let outer-loop ((chars (if ch (list ch) '())))
      (define ch (read-char port))
      (if (eof-object? ch)
        (values (reverse-list->string chars)
                'eof
                #f)
        (let inner-loop ((testers testers))
          (if (null? testers)
            (outer-loop (cons ch chars))
            (if (token-tester-feed (car testers) ch)
              (values (reverse-list->string (drop chars
                                                  (-
                                                    (string-length (token-tester-token (car testers)))
                                                     1)))
                      (token-tester-tag (car testers))
                      (token-tester-token (car testers)))
              (inner-loop (cdr testers)))))))))

(define (char-raw-literal-punc? c)
    (not (or (char-numeric? c)
             (char-alphabetic? c)
             (char=? c #\{)
             (char=? c #\})
             (char=? c #\@))))

(define (parse-guile-string port ch)
  ; use the guile reader
  (unget-char port ch)
  (read port))

(define (parse-guile-number port ch)
  (string->number (read-token port ch)))

(define (parse-guile-identifier port ch)
  (cond
    ((char=? ch #\|)
     (let ((ident
             (string->symbol
               (read-until port (read-char port)
                           (lambda (ch) (char=? ch #\|))))))
       (read-char port)
       ident))
    ((char-initial? ch)
     (string->symbol (read-until port ch
                                 (lambda (ch)
                                   (not (char-subsequent? ch))))))
    (else (error "illegal identifier"))))

(define (parse-guile-form port rdelim)

  (let loop ((items '()))

    (define (handle-final-cons)
      (read-whitespace port #f)
      (let ((expr (parse-guile-expr port (read-char port))))
        (when (not (char=? (read-char port)
                           rdelim))
          (error "expecting form close character"))
        ; this append will remove one layer of pairness, so if pair, then wrap in list
        (append (reverse items) (if (pair? expr)
                                  (list expr)
                                  expr))))


    (read-whitespace port #f)
    (let ((ch (read-char port)))
      (cond
        ((char=? ch rdelim) (reverse items))
        ((and (char=? ch #\.)
              (char-delimiter? (peek-char port)))
         (handle-final-cons))
        (else
          (loop (cons (parse-guile-expr port ch) items)))))))

; ch is always #
(define (parse-guile-hash port ch)
  (let ((ch (read-char port)))
    (case ch
      ((#\:) (symbol->keyword (parse-guile-identifier
                                port
                                (read-char port))))
      (else (error "cannot parse this hash yet")))))


(define (char->string ch)
  (list->string (list ch)))

(define (parse-guile-peculiar-identifier-or-number port ch)
  (define pch (peek-char port))
  (case ch
    ((#\+ #\-)
     (cond
       ((char-delimiter? pch)
        (string->symbol (char->string ch)))
       ((char-digit? pch)
        ((if (char=? #\+ ch) + -) (parse-guile-number port (read-char port))))
       ((char-sign-subsequent? pch)
        (string->symbol (string-append (char->string ch)
                                        (read-until port (read-char port)
                                                    (lambda (ch)
                                                      (not (char-subsequent? ch)))))))
       ((char=? #\. pch)
        (string->symbol ((string-append (char->string ch)
                                         (symbol->string (parse-guile-peculiar-identifier-or-number
                                                           port
                                                           (read-char port)))))))
       (else (error "illegal peculiar identifier"))))
    ((#\.)
     (unless (char-dot-subsequent? pch)
       (error "illegal peculiar identifier"))
     (string->symbol (string-append (char->string ch)
                                     (read-until port (read-char port)
                                                 (lambda (ch)
                                                   (not (char-subsequent? ch)))))))
    (else (error "impossible"))))

(define (get-literal-text-beginning-punc port ch)
  (let ((puncs (read-until port #f (negate char-raw-literal-punc?))))
    (if (not (char=? (peek-char port) #\{))
      (begin
        (do-ec (:range i
                       (- (string-length puncs) 1)
                       -1
                       -1)
               (unget-char port (string-ref puncs i)))
        #f)
      (begin
        (read-char port)
        puncs))))


(define (parse-text-tree-with-tokens port
                                     at-token
                                     open-curly-token
                                     close-curly-token)
  (parameterize ((at-token-param at-token)
                 (open-curly-token-param open-curly-token)
                 (close-curly-token-param close-curly-token))
    (parse-text-tree port)))


(define (make-reverser)
  (let* ((data '((#\( . #\))
                 (#\[ . #\])
                 (#\< . #\>)
                 (#\{ . #\})
                 (#\\ . #\/)))
         (table (fold (lambda (kar kons)
                        (cons (cons (cdr kar) (car kar))
                              (cons kar kons)))
                      '()
                      data)))
    (lambda (ch)
      (let ((pair (assv-ref table ch)))
        (if pair
          pair
          ch)))))

(define punc-reverser (make-reverser))

(define (mirror-and-reverse-punctuation chars)
  (string-reverse (string-map
                    punc-reverser 
                    chars)))

(define (parse-new-tokens-text-tree port literal-text-beginning-punc)
  (let ((at-token
          (string-append "|"
                         literal-text-beginning-punc
                         "@"))
        (open-curly-token
          (string-append "|"
                         literal-text-beginning-punc
                         "{"))
        (close-curly-token
          (string-append "}"
                         (mirror-and-reverse-punctuation literal-text-beginning-punc)
                         "|")))
    (parse-text-tree-with-tokens
      port
      at-token
      open-curly-token
      close-curly-token)))

(define (parse-guile-new-token-text-tree-or-identifier port ch)
  (if (not (char=? ch #\|))
    (parse-guile-identifier port ch)
    (let ((literal-text-beginning-punc
            (get-literal-text-beginning-punc port ch)))
      (if (not literal-text-beginning-punc)
        (parse-guile-identifier port ch)
        (parse-new-tokens-text-tree port literal-text-beginning-punc)))))

(define (parse-guile-new-token-text-tree port ch)
  ; ch always is | here
  (let ((literal-text-beginning-punc
          (get-literal-text-beginning-punc port ch)))
    (if (not literal-text-beginning-punc)
      (error "Syntax error here" port)
      (parse-new-tokens-text-tree port literal-text-beginning-punc))))


(define (parse-guile-expr port ch)

  (case ch
    ((#\") (parse-guile-string port ch))
    ((#\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\0)
     (parse-guile-number port ch))
    ((#\- #\+ #\.) (parse-guile-peculiar-identifier-or-number port ch))
    ((#\() (parse-guile-form port #\)))
    ((#\[) (parse-guile-form port #\]))
    ((#\@) (parse-free-guile-expr port ch))
    ((#\#) (parse-guile-hash port ch))
    ((#\') (list 'quote (parse-guile-expr port (read-char port))))
    ((#\`) (list 'quasiquote (parse-guile-expr port (read-char port))))
    ((#\,) (let ((sym (if (char=? (peek-char port) #\@)
                        'unquote-splicing
                        'unquote)))
             (when (eq? sym 'unquote-splicing)
               (read-char port)) ; get rid of the at sign
             (list sym (parse-guile-expr port (read-char port)))))
    (else (parse-guile-identifier port ch))))

; ch will always be @
(define (parse-free-guile-expr port ch)

  (letrec ((loop (lambda (expr args)
                   (define (return-value args)
                     (make-@expr (cond
                                   ((and (not expr)
                                         (not args)) (error "Useless @expr" port))
                                   ((not expr) (reverse args))
                                   ((not args) expr)
                                   (else (cons expr (reverse args))))))
                   (define (new-args new-arg)
                     (cond
                       ((not args)
                        (cond
                          ((null? new-arg) '())
                          ((list? new-arg) (reverse new-arg))
                          (else (list new-arg))))
                       ((null? new-arg) args)
                       ((list? new-arg) (append (reverse new-arg) args))
                       (else (cons new-arg args))))

                   (define ch (peek-char port))
                   (cond
                     ((eof-object? ch) (return-value args))
                     ((char=? ch #\|)
                      (read-char port)
                      (return-value (new-args (parse-guile-new-token-text-tree
                                                port
                                                ch))))
                     ((char=? ch #\{)
                      (read-char port)
                      (return-value (new-args (parse-text-tree port))))
                     ((char=? ch #\[)
                      (read-char port)
                      (loop expr (new-args (parse-guile-form port #\]))))
                     (else (return-value args)))))
           (ch (read-char port)))
    (case ch
      ((#\{) (make-@expr (parse-text-tree port)))
      ((#\() (parse-guile-expr port ch))
      ((#\[) (loop
               #f
               (reverse (parse-guile-form port #\]))))
      ((#\|)
       (let ((res (parse-guile-new-token-text-tree-or-identifier port ch)))
         (if (symbol? res); identifier!
           (loop res #f)
           res)))
      (else
        (loop (parse-guile-expr port ch)
              #f)))))

(define (cons-string-combine value tree)
  ; if the current value in the tree is a string, and the
  ; previous value (if there was one) is a string, then
  ; append them together for the new tree
  (cond
    ((and (string? value)
          (and (not (null? tree))
               (string? (car tree))))
     (cons (string-append (car tree)
                          value)
           (cdr tree)))
    ; otherwise, just add the new value to the tree
    (else (cons value tree))))

; parses a string from the port, with balanced parentheses
(define (parse-text-tree port)
  (let-values (((initial-tree-item
                  special-char-type
                  token) (parse-free-guile-text port #f))
               ((balanced-count) 0))
    (let loop ((tree (if (= (string-length initial-tree-item) 0)
                       '()
                       (list initial-tree-item))) ; we hit something immediately
               (special-char-type special-char-type) ; can be #f | 'close | 'end | 'at
               (token token)) 

      (define unbalanced? (not (= balanced-count 0)))
      (set! balanced-count (case special-char-type
                             ((open-curly) (+ balanced-count 1))
                             ((close-curly) (- balanced-count 1))
                             (else balanced-count)))

      (cond 
        ((and (eq? special-char-type 'eof)
              unbalanced?) (error "unbalanced curly braces"))
        ((= balanced-count -1) (reverse tree))
        ((or (eq? special-char-type 'eof)
             (and (eq? special-char-type 'close-curly)
                  (not unbalanced?))) (reverse tree))
        (else
          ; at this point, it cannot be eof.
          ; if open-curly close-curly, then it's a balanced one (or the start
          ; of a potentially balanced one). Add it!
          ; if it's at, then we shouldn't add that token to the tree.
          (unless (eq? special-char-type 'at)
            (set! tree (cons-string-combine token tree)))
          
          (let-values (((new-vals special-char-type token)
                        (case special-char-type
                          ((at)
                           (let*-values (((expr-val) (parse-free-guile-expr port #f))
                                         ((free-text-val
                                            special-char-type
                                            token) (parse-free-guile-text port #f)))
                             (values (cond
                                       ((= (string-length free-text-val) 0) (list expr-val))
                                       (else (list expr-val free-text-val)))
                                     special-char-type
                                     token)))
                          (else
                            (let-values (((new-val special-char-type token)
                                          (parse-free-guile-text port #f)))
                              (values (list new-val) special-char-type token))))))
            (loop
              (fold cons-string-combine tree new-vals)
              special-char-type
              token)))))))

; Applies the transformation to every node in the tree.
;
; NOTE: Does not tranform the return value of the transformation function. It
; that's needed, use a recursive call in the transformation function.

(define (map-ast-rec proc node)
    (cond
      [(null? node) '()]
      [(pair? node) (cons (map-ast-rec proc (car node))
                          (map-ast-rec proc (cdr node)))]
      [else         (proc node)]))

; Adds @expr-invocation macros whereever an @expr{content} was used.
(define (handle-@exprs x)
  (define (handler val)
    (cond
     ; If the expression is a list, this it's an @expr invocation, and needs to
     ; have the macro appended to it.
      [(@expr? val) (handle-@exprs (let ((e (@expr-expr val)))
                                     (if (pair? e) ;  if function call?
                                       (cons '@expr-invocation e)
                                       e)))]
      [else val]))
  (map-ast-rec handler x))


(define (nectar-include-form? x)
  (match x
    (('nectar-include (? string?)) #t)
    (else #f)))

(define (get-tree-from-file filename)
  (call-with-input-file filename parse-raw-tree))

; This handles the presence of (nectar-include "name-of-file.scm") forms in the
; file. The file's contents will be spliced into the region, like the contents
; of that file were splatted into that region.
(define (handle-nectar-include tree)
  (match tree
    (() '())
    (((? nectar-include-form? x) . rest) (append (get-tree-from-file (cadr x))
                                                 (handle-nectar-include rest)))
    ((x . y) (cons x (handle-nectar-include y)))))

(define (parse-raw-tree port)
  (let ((tree (parse-text-tree port)))
    (if (eof-object? (read-char port))
      tree
      (error "unbalanced curly braces"))))

; Adds references to the @expr-invocation macros whereever there is an @expr,
; and splices in nectar-includes into the input.
(define (post-process-raw-tree tree)
  (handle-@exprs (handle-nectar-include tree)))

(define-public (port->nectar port)
               (cons '@expr-invocation-top-level
                     (post-process-raw-tree (parse-raw-tree port))))

(define-public (file->nectar filename)
               (call-with-input-file filename port->nectar))

(define-public (string->nectar str)
               (call-with-input-string str port->nectar))
