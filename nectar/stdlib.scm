(define-module (nectar stdlib)
               #:export (tag-lambda
                          declare-tags
                          define-tag
                          @expr-invocation
                          @expr-invocation-top-level
                          nectar-current-filename
                          nectar-ast-transform
                          nectar-ast-fold))


(use-modules
  (srfi srfi-1)
  (nectar utils)
  (nectar tag)
  (nectar evaluate)
  (system syntax))

(re-export-modules (nectar tag))

; tag-lambda

; Makes a function that takes a variable number of arguments, collects the
; keywords in front of the arguments, and passes them as properties and
; children to its

; fn should take attrs and children
(define (tag-lambda fn)
  (lambda args (call-with-values (lambda () (collect-keywords args))
                                 fn)))

(define-syntax declare-tags
  (syntax-rules ()
    ((declare-tags e1 ...)
     (begin
       (define e1 (lambda args
                    (call-with-values
                      (lambda () (collect-keywords args))
                      (lambda (kws children)
                        (make-tag 'e1 kws children))))) ...))))


(define-syntax define-tag
  (syntax-rules ()
    ((define-tag (name attrs children) body body* ...)
     (define name
       (tag-lambda (lambda (attrs children)
                     body body* ...))))))

(define-syntax reverse-list
  (syntax-rules ()
    ((_ k () new-list)
     (k new-list))
    ((_ k (v . others) new-list)
     (reverse-list k others (v . new-list)))))

(define-syntax reverse-list-apply
  (syntax-rules ()
    ((_ k lst)
     (let-syntax ((next (syntax-rules ()
                          ((_ (v . others))
                           (k v . others))
                          ((_ ())
                           (k)))))
       (reverse-list next lst ())))))

                                        ; (_ cont variable-name-state body-exprs)
(define-syntax nectar-eval-macro-1
  ;; This macro needs to special case use-modules, because it doesn't work like other constructs.

  ;; For example, Guile fails to properly expand match in this case

  ;; (let () (use-modules (ice-9 match)) (let () (match '(1 2) ((a b) a))))

  ;; It recognizes that match is an imported binding from ice-9 match,
  ;; but nevertheless doesn't recognize it as a macro

  ;; To forego this odd behavior, we are going to rewrite use-modules
  ;; to use module-use!

  ;; Thanks stis from irc! Date: 2022-02-02

  (let-syntax ((make-macro-with-exceptions (syntax-rules ()
                                             ((_ literals ...)
                                              (lambda (stx)
                                                (with-ellipsis :::
                                                               (syntax-case stx (use-modules literals ...)
                                                                 ((_ k variable-names ())
                                                                  #'(reverse-list-apply k variable-names))
                                                                 ((_ k variable-names ((use-modules specs :::)  . exprs))
                                                                  ;; We need to use this during expand time too for it to work
                                                                  (for-each
                                                                   (lambda (spec)
                                                                     (module-use! (current-module) (resolve-interface spec)))
                                                                   (syntax->datum #'(specs :::)))
                                                                  #'(begin
                                                                      (module-use! (current-module) (resolve-interface 'specs)) :::
                                                                      (nectar-eval-macro-1 k variable-names exprs)))
                                                                 ;; if literal, then, execute it without capture
                                                                 ((_ k variable-names ((literals . others) . exprs))
                                                                  #'(let ()
                                                                      (literals . others)
                                                                      (nectar-eval-macro-1 k variable-names exprs))) ...
                                                                 ((_ k variable-names (expr . exprs))
                                                                  #'(let ((v expr))
                                                                      (nectar-eval-macro-1 k (v . variable-names) exprs))))))))))
    (make-macro-with-exceptions define define* define-public define-syntax define-syntax-rule
                                library import define-tag)))


(define-syntax @expr-invocation-1
  (lambda (stx)
    (define (in-module? name)
      (module-variable (current-module) (syntax->datum name)))

    (define (compile-time-defined? name)
      (case (syntax-local-binding name)
        ((global) #f) ; the default is global
        (else #t)))

    (define (should-direct-call? name)
      (or (compile-time-defined? name)
          (in-module? name)))

    (syntax-case stx ()
      ((@expr-invocation name . args)
       (if (should-direct-call? #'name)
           #'(name . args)
           #'(if (defined? 'name)
                 (name . args)
                 (let ((anonymous-tag (tag-lambda (lambda (attrs children)
                                                    (make-tag 'name attrs children)))))
                   (anonymous-tag . args))))))))

(define-syntax @expr-invocation
  (syntax-rules ()
    ((_ name . body)
     (let-syntax ((next (syntax-rules ()
                            ((_ . args-1)
                             (@expr-invocation-1 name . args-1)))))
         (nectar-eval-macro-1 next () body)))))

(define-syntax @expr-invocation-top-level
  (syntax-rules ()
    ((_ . args)
     (nectar-eval-macro-1 list () args))))

(define nectar-current-filename (make-parameter #f))

; (nectar-ast-transform node transformer)
; tree: an ast node
; transformer: a (lambda (process node)) where process: (lambda (node))
; recurses through the element and called the transformer on it

; the user should call process with a node (after optionally transforming it)
; to recurse over the node (if a tag or a list) and apply the transformer.
; the user can choose not to recursively transform an element by not calling
; process with it
(define (nectar-ast-transform node transformer)
  (let ((process (lambda (item)
                   (cond
                     ((pair? item) (map
                                     (lambda (x)
                                       (nectar-ast-transform x transformer))
                                     item))
                     ((tag? item) (tag-with-children item (nectar-ast-transform (tag-children item) transformer)))
                     (else item)))))
    (transformer process node)))


; (nectar-ast-fold proc ctx node)
; proc: (lambda (process node ctx)), where process: (lambda (node ctx))
; A generalization of fold for asts.
; The user can call (process node ctx) to recurse over an element
(define (nectar-ast-fold proc ctx node)
  (letrec ((process (lambda (node ctx)
                      (cond
                        ((pair? node) (fold
                                        (lambda (item ctx)
                                          (nectar-ast-fold proc ctx item))
                                        ctx
                                        node))
                        ((tag? node) (process (tag-children node) ctx))
                        (else ctx)))))
    (proc process node ctx)))

; A tag for all renderers, should put the text in the output without escaping.
(declare-tags passthrough)
