(define-module (nectar utils))

(use-modules (srfi srfi-1)
             (nectar tag))


(define-syntax re-export-modules
  (syntax-rules ()
    ((_ (mod ...) ...)
     (begin
       (module-use! (module-public-interface (current-module))
                    (resolve-interface '(mod ...)))
       ...))))

(export re-export-modules)

; collects the keywords at the beginning of a function call,
; and convert them to symbols, returning an alist
(define-public (collect-keywords args)
  (let loop ((args args)
             (keywords '()))
    (cond
      ((or (null? args)
           (not (keyword? (car args)))) (values keywords args))
      ((keyword? (car args))
       (let ((keyword (car args))
             (value (cadr args))
             (rest (cddr args)))
         (loop rest (cons (cons (keyword->symbol keyword) value) keywords)))))))


(define-public (flat-map proc lst)
  (fold-right
    (lambda (kar kdr)
      (let ((new-val (proc kar)))
        (if (pair? new-val)
          (append new-val kdr)
          (cons new-val kdr))))
    '()
    lst))

(define-public (flatten-ast element)
  (cond
    ((string? element) element)
    ((tag? element) (make-tag (tag-name element)
                              (tag-attrs element)
                              (flatten-ast (tag-children element))))
    ((pair? element) (let loop ((ast element)
                                (new-ast '()))
                       (cond
                         ((null? ast) (reverse new-ast))
                         ((pair? (car ast)) (loop (cdr ast)
                                                  (append-reverse (flatten-ast (car ast)) new-ast)))
                         (else (loop (cdr ast)
                                     (cons (flatten-ast (car ast))
                                           new-ast))))))
    (else element)))


; returns true when the pattern matches
(define-public (make-incremental-string-detector str)
               (let ((idx 0)
                     (len (string-length str)))
                 (lambda (ch)
                   (when (>= idx len)
                     (set! idx 0))
                   (if (char=? ch (string-ref str idx))
                     (begin 
                       (set! idx (+ idx 1))
                       (if (= idx len) #t #f))
                     (begin
                       (set! idx 0)
                       #f)))))
