(define-module (nectar converters markdown))

(use-modules (sxml simple)
             (ice-9 match)
             (ice-9 binary-ports)
             (ice-9 popen))

(export markdown-string->nectar-string
        markdown-port->nectar-string)

(define !children
  (match-lambda
    ((name ('@ . attrs) . children) children)
    ((name . children) children)
    (x (error "Can't understand" x))))

(define element? pair?)

(define !attrs
  (match-lambda
    ((name ('@ . attrs) . children) attrs)
    ((name . children) '())))

(define (with-children el children)
  (match el
    ((name ('@ . attrs) . others) (cons name (cons (cons '@ attrs) children)))
    ((name . others) (cons name children))))


(define* (wrap-render beg end el #:optional (tight #t))
  (list beg
        (render-commonmark-el-lst (!children el) tight)
        end))

(define* (render-commonmark-el el #:optional (tight #t))
  
  (if (string? el)
      el
      (case (car el)
        ((http://commonmark.org/xml/1.0:thematic_break) "@hr{}")
        ((http://commonmark.org/xml/1.0:heading) (wrap-render
                                                  (string-append "@h" (car (assq-ref (!attrs el) 'level))
                                                                 "{")
                                                  "}"
                                                  el))
        ((http://commonmark.org/xml/1.0:document) (render-commonmark-el-lst (!children el) tight))
        ((http://commonmark.org/xml/1.0:text) (caddr el))
        ((http://commonmark.org/xml/1.0:paragraph)
         (if (and (= (length (!children el)) 1)
                  (not (eq? (car (car (!children el))) 'http://commonmark.org/xml/1.0:text)))
             (wrap-render "@p{"
                          "}"
                          el)
             (wrap-render ""
                          ""
                          el)))
        ((http://commonmark.org/xml/1.0:softbreak) "\n")
        ((http://commonmark.org/xml/1.0:linebreak) (list " @br{}" (if tight "" "\n")))
        ((http://commonmark.org/xml/1.0:emph) (wrap-render "@em{" "}" el))
        ((http://commonmark.org/xml/1.0:strong) (wrap-render "@b{" "}" el))
        ((http://commonmark.org/xml/1.0:link) (wrap-render
                                               (string-append "@a[#:href \""
                                                              (car (assq-ref (!attrs el) 'destination))
                                                              "\"]{")
                                               "}" el))
        ((http://commonmark.org/xml/1.0:block_quote) (wrap-render
                                                      "@blockquote{"
                                                      "}"
                                                      el))
        ((http://commonmark.org/xml/1.0:list)
         (wrap-render
          (if (string=? (car (assq-ref (!attrs el) 'type)) "bullet")
              "@ul{"
              "@ol{")
          "}"
          el
          (string=? (car (assq-ref (!attrs el) 'type)) "true")))
        ((http://commonmark.org/xml/1.0:item) (if tight
                                                  (wrap-render "@li{@inline{" "}}" el tight)
                                                  (wrap-render "@li{" "}" el tight)))
        ((http://commonmark.org/xml/1.0:code) (wrap-render "@code{" "}" el))
        ((http://commonmark.org/xml/1.0:code_block)
         (let ((info (assq-ref (!attrs el) 'info)))
           (wrap-render
            (if info
                (string-append "@pre{@code[#:class \"language-" (car info) "\"]{")
                "@pre{@code{")
            "}}"
            el)))
        (else (error "Can't render " (car el))))))

(define* (render-commonmark-el-lst lst #:optional (tight #t))
  (let* ((renderings (map (lambda (x)
                            (render-commonmark-el x tight))
                          lst)))
    (if (not tight)
        (if (null? renderings)
            '()
            (cons (car renderings) (map (lambda (x) (list "\n\n" x)) (cdr renderings))))
        renderings)))

(define (no-strings lst)
  (filter
   (negate string?)
   lst))

(define (remove-unnecessary-strings el)
  (if (and (element? el)
           (not (eq? (car el) 'http://commonmark.org/xml/1.0:text))
           (not (eq? (car el) 'http://commonmark.org/xml/1.0:code_block))
           (not (eq? (car el) 'http://commonmark.org/xml/1.0:code)))
      (with-children el (map
                         remove-unnecessary-strings
                         (no-strings (!children el))))
      el))


(define (flatten-nested-lists lst)
  (cond
   ((null? lst) '())
   ((pair? (car lst)) (append (flatten-nested-lists (car lst)) (flatten-nested-lists (cdr lst))))
   (else (cons (car lst) (flatten-nested-lists (cdr lst))))))

(define (cmark-xml->nectar-string port/string)
  (let* ((document (list-ref (xml->sxml port/string) 2)))
    (string-concatenate
     (flatten-nested-lists
      (render-commonmark-el
       ;; strings in the xml, but not in the original document.
       (remove-unnecessary-strings document)
       #f)))))

(define (markdown-port->nectar-string port)
  (let* ((out-port (mkstemp "/tmp/nectar-XXXXXX"))
         (filename (port-filename out-port)))
    (put-bytevector out-port (get-bytevector-all port))
    (close out-port)
    (let* ((pipe (open-pipe* OPEN_READ "cmark" "-t" "xml" filename)))
      (let ((ret (cmark-xml->nectar-string pipe)))
        (close-pipe pipe)
        (delete-file filename)
        ret))))

(define (markdown-string->nectar-string str)
  (unless (string? str)
    (error "was expecting string" str))
  (let* ((out-port (mkstemp "nectar-XXXXXX"))
         (filename (port-filename out-port)))
    (display str out-port)
    (close out-port)
    (let* ((pipe (open-pipe* OPEN_READ "cmark" "-t" "xml" filename)))
      (let ((ret (cmark-xml->nectar-string pipe)))
        (close-pipe pipe)
        (delete-file filename)
        ret))))
