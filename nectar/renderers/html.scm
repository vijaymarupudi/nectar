(define-module (nectar renderers html))

(use-modules (nectar renderers html internal)
             (nectar tag)
             (nectar utils)
             (nectar renderers plain-text)
             (srfi srfi-1))

(export nectar-ast->html
        nectar-html-escape-string)

(define (escaped-chars c)
  (case c
    ((#\<) (reverse (string->list "&lt;")))
    ((#\>) (reverse (string->list "&gt;")))
    ((#\&) (reverse (string->list "&amp;")))
    ((#\") (reverse (string->list "&quot;")))
    (else #f)))

(define (char-needs-escaping? c)
  (case c
    ((#\< #\> #\& #\") #t)
    (else #f)))

(define (string-find-index str pred)
  (define len (string-length str))
  (let loop ((i 0))
    (if (not (< i len))
      #f
      (if (pred (string-ref str i))
        i
        (loop (+ i 1))))))

(define (nectar-html-escape-string x)
  (let ((begin-idx (string-find-index x char-needs-escaping?)))
    (if (not begin-idx)
      x
      (let ((len (string-length x)))
        (let loop ((i begin-idx)
                   (l '()))
          (if (not (< i len))
            (string-append (substring x 0 begin-idx)
                           (reverse-list->string l))
            (loop (+ i 1)
                  (let ((chars (escaped-chars (string-ref x i))))
                    (if chars
                      (append chars l)
                      (cons (string-ref x i) l))))))))))

(define (should-always-be-closed? tag-name)
  (case tag-name
    ((script) #t)
    (else #f)))

(define (can-self-close? tag-name schildren)
  (and (not (should-always-be-closed? tag-name))
       (null? schildren)))

(define (tag-renderer x)
  (cond
   ((eq? (tag-name x) 'passthrough) (nectar-ast->plain-text (tag-children x)))
   ((eq? (tag-name x) 'inline) (render-html (tag-children x)))
    (else
      (let ([name (tag-name x)]
            [attrs (tag-attrs x)]
            [children (tag-children x)])

    (let* ([sattrs (if (null? attrs) attrs
                     (string-concatenate (map
                                           (lambda (x)
                                             (format #f " ~a=\"~a\"" (symbol->string (car x))
                                                     (nectar-html-escape-string (nectar-ast->plain-text (cdr x)))))
                                           attrs)
                                         ))]
           [schildren (if (null? children)
                        children
                        (if (metadata-content? x)
                          (nectar-ast->plain-text children)
                          (render-html children)))])
      (string-append "<" (symbol->string name)
                     (if (null? sattrs) "" sattrs)
                     (if (can-self-close? name schildren) "/>" ">")
                     (if (can-self-close? name schildren) "" (if (null? schildren) "" schildren))
                     (if (can-self-close? name schildren)
                       ""
                       (string-append "</"
                                      (symbol->string name)
                                      ">"))))))))

(define (render-html x)
  (cond
   [(string? x) (nectar-html-escape-string x)]
   [(tag? x) (tag-renderer x)]
   [(pair? x) (string-concatenate (map render-html x))]
   [(number? x) (number->string x)]
   [(null? x) ""]
   [else (error "Cannot render-html" x)]))

(define* (nectar-ast->html ast #:key (paragraph-break? #t))
  (let* ((x (flatten-ast ast))
         (x (if paragraph-break? (pbreak-ast x) x))
         (x (render-html x)))
    x))
