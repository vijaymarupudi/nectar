(define-module (nectar renderers plain-text))

(use-modules (nectar tag))

(define (render-plain-text x)
  (cond
    [(string? x) x]
    [(tag? x) (error "Cannot render tag to text" x)]
    [(pair? x) (string-concatenate (map render-plain-text x))]
    [(number? x) (number->string x)]
    [else (error "Cannot render " x)]))

(define-public nectar-ast->plain-text render-plain-text)
