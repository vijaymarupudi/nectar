(define-module (nectar renderers html internal predicates))
(use-modules (nectar stdlib))

(define-public (phrasing-content? element)
  (cond
    ((string? element) #t)
    ((not (tag? element)) (error "Invalid input to function" element))
    (else (case (tag-name element)
            ((
              a
              abbr 
              audio 
              b 
              bdi 
              bdo 
              br 
              button 
              canvas 
              cite 
              code 
              command 
              datalist 
              dfn 
              em 
              embed 
              i 
              iframe 
              img 
              input 
              kbd 
              keygen 
              label 
              mark 
              math 
              meter 
              noscript 
              object 
              output 
              progress 
              q 
              ruby 
              s 
              samp 
              script 
              select 
              small 
              span 
              strong 
              sub 
              sup 
              svg 
              textarea 
              time 
              u 
              var 
              video 
              wbr
              ) #t)
              ; TODO: These elements are valid if their children are phrasing content
              ; area  
              ; del  
              ; ins  
              ; map  
            (else #f)))))


(define-public (heading-content? element)
  (and (tag? element)
       (case (tag-name element)
         ((h1 h2 h3 h4 h5 h6 hgroup) #t)
         (else #f))))

(define-public (sectioning-content? element)
  (and (tag? element)
       (case (tag-name element)
         ((article aside nav section) #t)
         (else #f))))

(define-public (metadata-content? node)
               (and (tag? node)
                    (case (tag-name node)
                      ((title meta link script style) #t)
                      (else #f))))

 

; Old code, moved to a more HTML5 compliant mechanism

; (define-public (block-tag? x)
;                (case (tag-name x)
;                  ((abbr
;                     audio
;                     b
;                     bdo
;                     br
;                     button
;                     canvas
;                     cite
;                     code
;                     command
;                     data
;                     datalist
;                     dfn
;                     em
;                     embed
;                     i
;                     iframe
;                     img
;                     input
;                     kbd
;                     keygen
;                     label
;                     mark
;                     math
;                     meter
;                     noscript
;                     object
;                     output
;                     picture
;                     progress
;                     q
;                     ruby
;                     samp
;                     script
;                     select
;                     small
;                     span
;                     strong
;                     sub
;                     sup
;                     svg
;                     textarea
;                     time
;                     u
;                     var
;                     video
;                     a
;                     area
;                     del
;                     ins
;                     link
;                     map
;                     meta
;                     wbr) #f)
;                  (else #t)))
