; from https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories

(define-module (nectar renderers html internal))
(use-modules (nectar tag)
             (nectar utils)
             (srfi srfi-1)
             (nectar renderers html internal predicates))

(re-export metadata-content?)


(define pbreak-sign (gensym " pbreak"))

(define-public (pbreak-string str)

; proceed until
;   string
;     no newline in string -> continue
;     else
;       end paragraph
;       keep skipping newlines and whitespace
;       begin again
;   block element
;     end paragraph
;     process block element
;     keep skipping newlines and whitespace
;     begin again



  (define (exit l)
    (reverse l))

  (define (valid-string-index? idx)
    (< idx (string-length str)))

  (define (until-double-newline idx l)
    ; 1. if eos, then return
    ; 2. find double newline index
    ; 3. iterate until not whitespace or end of string


    (let ((nlidx (string-contains str "\n\n" idx)))
      (cond
        [nlidx (p-break-mode nlidx 
                             (if (not (= idx nlidx)) ; only add a string if there is some content in it
                               (cons (substring str idx nlidx) l)
                               l))]
        [else (exit (cons (substring str idx) l))])))

  (define (p-break-mode idx l)
    (let loop ((nlidx idx))
      (cond
        [(not (valid-string-index? nlidx))
         (exit (cons pbreak-sign l))]
        [(char-whitespace? (string-ref str nlidx))
         (loop (+ nlidx 1))]
        [else
          (until-double-newline nlidx (cons pbreak-sign l))])))

  (until-double-newline 0 '()))

(define (non-whitespace-index str)
  (string-index str (negate char-whitespace?)))

(define (consume-pbreaks lst)
  (let ((remaining (drop-while
                     (lambda (x) (cond
                                   [(eq? x pbreak-sign) #t]
                                   [(string? x) (not (non-whitespace-index x))]
                                   [else #f]))
                     lst)))
    (cond
      [(null? remaining) remaining]
      [(string? (car remaining)) (let ((idx (non-whitespace-index (car remaining))))
                                   (if (not idx)
                                     remaining
                                     (cons (substring (car remaining) idx)
                                           (cdr remaining))))]
      [else remaining])))


(define (might-have-paragraphs-inside? element)
  (not (or (string? element)
           (eq? (tag-name element) 'p)
           (eq? (tag-name element) 'passthrough)
           (eq? (tag-name element) 'inline)
           (heading-content? element)
           (phrasing-content? element)
           (metadata-content? element))))


(define (gather-paragraph lst)
  (span
    (lambda (x) (cond
                  [(tag? x) (if (might-have-paragraphs-inside? x) #f #t)]
                  [(eq? pbreak-sign x) #f]
                  [else #t]))
    lst))



(define (split-ps lst)
  (let loop ((l '())
             (lst (consume-pbreaks lst)))
    (cond
      [(null? lst) (reverse l)]
      [(and (tag? (car lst))
            (might-have-paragraphs-inside? (car lst))) (loop (cons (car lst) l) (cdr lst))]
      [else (call-with-values
              (lambda () (gather-paragraph lst))
              (lambda (contents rest)
                (loop
                  (cond
                    [(null? contents) l]
                    ; create paragraph only if there is non-whitespace free text
                    [(any (lambda (x)
                            (and (string? x)
                                 (non-whitespace-index x)))
                          contents) (cons (make-tag 'p '() contents) l)] 
                    [else (cons contents l)])
                  (consume-pbreaks rest))))])))


(define-public (pbreak-ast ast)

               (define (tag-handler node)
                 (cond
                   [(and (might-have-paragraphs-inside? node)
                         (not (metadata-content? node))
                         (not (eq? (tag-name node) 'p)))
                    (tag-with-children
                      node
                      (block-children-handler (tag-children node)))]
                   [else node]))

               (define (block-children-handler children)
                 (let* ((children (flat-map (lambda (x) (if (string? x) (pbreak-string x) x)) children))
                        (children (flat-map
                                    (lambda (node)
                                      (if (tag? node)
                                        (tag-handler node)
                                        node))
                                    children)))
                   (split-ps children)))

               ((if (tag? ast)
                  tag-handler
                  block-children-handler) ast))

