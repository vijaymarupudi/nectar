(define-module (nectar renderers latex))

(export nectar-latex-escape-string)

(define (flatten lst)
  (cond
   ((null? lst) lst)
   ((not (pair? (car lst))) (cons (car lst) (flatten (cdr lst))))
   (else (append (flatten (car lst)) (flatten (cdr lst))))))

(define (nectar-latex-escape-string rt)
  (list->string
   (flatten
    (reverse!
     (string-fold
      (lambda (c acc)
        (let-syntax ((simple-escape (syntax-rules ()
                                      ((_ c (chars ...) otherwise)
                                       (case c
                                         ((chars) (list #\\ chars)) ...
                                         (else (otherwise c)))))))
          (cons
           (simple-escape c (#\# #\$ #\% #\& #\~ #\{ #\})
                          (lambda (c)
                            (case c
                              ((#\^) (string->list "\textasciicircum{}"))
                              ((#\~) (string->list "\textasciitiltde{}"))
                              ((#\\) (string->list "\textbackslash{}"))
                              (else c))))
           acc)))
      '()
      rt)))))


