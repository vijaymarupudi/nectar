(define-module (nectar evaluate))

(use-modules
  (srfi srfi-1)
  (nectar stdlib)
  (nectar tag)
  (nectar parser))

; primitives for evaluation
(define UNSPECIFIED (if #f #f))
(define (filter-falsy data)
  (filter
    (lambda (x) (not (or (eq? x UNSPECIFIED)
                         (not x))))
    data))


(define (environment->nectar-environment! env)
  ; for the default guile environment
  (module-use! env (resolve-interface '(nectar stdlib))))

(define-public (make-nectar-environment)
  (let ((m (define-module* (list (gensym " nectar-eval-module")))))
    (environment->nectar-environment! m)
    m))

(define (internal-nectar->nectar-ast nectar nectar-environment)
  (filter-falsy (eval nectar nectar-environment)))

(define-public (nectar-with-bindings nectar bindings)
  `(let ,(map (lambda (pair) (list (car pair) (list 'quote (cdr pair))))
              bindings)
     ,nectar))


(define-public nectar->nectar-ast
               (lambda* (nectar #:optional (nectar-environment (make-nectar-environment)))
                 (internal-nectar->nectar-ast nectar nectar-environment)))

(define-public (nectar-ast->sxml ast)

  (define (handler node)
    (cond
     [(pair? node) (map handler node)]
     [(tag? node) (cons (tag-name node)
                        (if (not (null? (tag-attrs node)))
                            (cons (cons '@ (tag-attrs node)) (handler (tag-children node)))
                            (handler (tag-children node))))]
     [else node]))

  (handler (cons '*TOP* ast)))

(define-public nectar->sxml (compose nectar-ast->sxml nectar->nectar-ast))


(define-public (file->nectar-ast file . others)
               (call-with-input-file file
                                     (lambda (port)
                                       (apply port->nectar-ast port others))))

(define-public (string->nectar-ast str . others)
               (apply nectar->nectar-ast (string->nectar str) others))

(define-public (port->nectar-ast port . others)
               (parameterize ((nectar-current-filename (port-filename port)))
                 (apply nectar->nectar-ast (port->nectar port) others)))
