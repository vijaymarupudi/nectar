(define-module (nectar format))

(use-modules (nectar parser)
             (nectar evaluate)
             (nectar renderers plain-text))


(define-syntax nectar-format
  (lambda (stx)
    ;; why am I not using a recursive macro call instead? because guile renames
    ;; variables outside the macro when you do that
    (define (make-return port template)
      (with-syntax ((nectar (datum->syntax stx (string->nectar (syntax->datum template)))))
        (syntax-case port ()
          (#f #'(nectar-ast->plain-text nectar))
          (#t #'(display (nectar-ast->plain-text nectar) (current-output-port)))
          (else #'(display (nectar-ast->plain-text nectar) port)))))
    (syntax-case stx ()
      ((nectar-format port template)
       (make-return #'port #'template)))))

(export nectar-format)
