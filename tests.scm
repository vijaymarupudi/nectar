(use-modules (nectar)
             (nectar utils)
             (nectar renderers html)
             (nectar converters markdown)
             (srfi srfi-64)
             (srfi srfi-1)
             (ice-9 match))

(define-syntax-rule (nectar-test items ... str out)
                    (test-equal items ...
                                ; the car is @expr-invocation-top-level
                                (cdr (string->nectar str))
                                (quote out)))

(define (make-ast item)
  (match item
         (((? symbol? tag-name) ('@ props ...) children ...)
          (make-tag tag-name props (map make-ast children)))
         (((? symbol? tag-name) children ...)
          (make-tag tag-name '() (map make-ast children)))
         ((items ...) (map make-ast items))
         (else item)))

(define-syntax-rule (ast item)
                    (make-ast (quote item)))

(define-syntax-rule (ast-test str ast-input)
                    (test-equal (nectar->nectar-ast (string->nectar str))
                                (ast ast-input)))

(define-syntax-rule (html-renderer-test ast-input output)
                    (test-equal (nectar-ast->html (ast ast-input))
                                output))


(define (parser-tests)


  (test-begin "parser-test")

  (test-begin "new-token-test")

  (nectar-test "@test|..{ha}..|"
               ((@expr-invocation test "ha")))

  (nectar-test "@style{@|{test}|}"
               ((@expr-invocation style ("test"))))


  (nectar-test "@|..{nice}..|"
               (("nice")))


  (nectar-test "@|..{ni@|i|ce}..|"
               (("ni@|i|ce")))


  (nectar-test "@|..{ni|..@|i|ce}..|"
               (("ni" i "ce")))


  ; reversing punc indicator
  (nectar-test "@|<.{ni|<.@|i|ce}.>|"
               (("ni" i "ce")))


  (nectar-test "@|<.{ni|<..ce}.>|"
               (("ni|<..ce")))

  ; balanced curlies in literal
  (nectar-test "@|<.{test|<.{}.>|ing}.>|"
               (("test|<.{}.>|ing")))

  (test-end "new-token-test")

  (test-begin "balanced-curlies")
  (nectar-test "this {is a link}"
               ("this {is a link}"))

  (nectar-test "this {{{is a link}}}"
               ("this {{{is a link}}}"))


  (nectar-test "this {{ nice {is a link}}}"
               ("this {{ nice {is a link}}}"))
  (test-end "balanced-curlies")

  (test-begin "@expr-test")

  (nectar-test "@a[]{nice}"
               ((@expr-invocation a "nice")))

  (nectar-test "@a[]{}"
               ((@expr-invocation a)))

  (nectar-test "@|a|"
               (a))

  (nectar-test "@|a||{nice}|"
               ((@expr-invocation a "nice")))


  (nectar-test "@[a]{nice}"
               ((@expr-invocation a "nice")))

  (test-end "@expr-test")

  (nectar-test "this @a{is a link}"
               ("this " (@expr-invocation a "is a link")))


  (nectar-test "this @a[#:href \"link\"]{is a link}"
               ("this " (@expr-invocation a #:href "link" "is a link")))

  (nectar-test "@(define name \"Rina\")@name is the best!"
            ((define name "Rina") name " is the best!"))


  (nectar-test "@(let loop () 4)"
               ((let loop () 4)))

  (nectar-test "@|name|"
               (name))

  (nectar-test "@|.?name|"
               (.?name))

  (nectar-test "@(nectar-include \"tests/test-include-file.nct\")@thing"
               ((define thing "nice") "\n" thing))

  (test-end "parser-test"))

(define (evaluation-tests)
  (test-begin "evaluation-test")

  (ast-test "@a{}"
            ((a)))

  (ast-test "@a[#:href \"testing\"]{}"
            ((a (@ (href . "testing")))))

  (ast-test "@a[#:href \"testing\"]{test1 @b{test2}"
            ((a (@ (href . "testing")) "test1 " (b "test2"))))

  (ast-test "@(define name \"Rina\")@name is the best!"
            ("Rina" " is the best!"))

  (use-modules (language tree-il)
               (ice-9 pretty-print))

  (ast-test "@(use-modules (ice-9 match))@(match '(1 2)
                                           ((a b) a))"
            (1))

  (test-end "evaluation-test")

  )

(define (renderer-tests)
  (test-begin "rendering-test")
  (html-renderer-test (br) "<br/>")
  (html-renderer-test (a (@ (href . "https://google.com")) "link") "<a href=\"https://google.com\">link</a>")

  ; testing quotes in attributes and text
  (html-renderer-test (div (@ (data-nice . "wo\"wo")) "link")
                      "<div data-nice=\"wo&quot;wo\"><p>link</p></div>")
  (html-renderer-test (div "test\"test")
                      "<div><p>test&quot;test</p></div>")

  ; expr combination in attributes
  (html-renderer-test (div (@ (data-nice . ("nice " ("there" 2))))) "<div data-nice=\"nice there2\"/>")

  (test-begin "paragraph-breaking-test")

  (html-renderer-test (html (head (title "Nice")) (body "\n  " (h1 "content") "\n\nNice"))
                      "<html><head><title>Nice</title></head><body><h1>content</h1><p>Nice</p></body></html>")

  ; Nested list
  (html-renderer-test (html (head (title "Nice")) (body "\n  " ((h1 "content") "\n\nNice")))
                      "<html><head><title>Nice</title></head><body><h1>content</h1><p>Nice</p></body></html>")

  (test-end "paragraph-breaking-test")

  (test-begin "passthrough-test")

  (html-renderer-test (passthrough "<<<illegal")
                      "<<<illegal")

  (test-end "passthrough-test")

  (test-begin "inline-test")

  (html-renderer-test (ul (li "testing") (li (inline "testing")))
                      "<ul><li><p>testing</p></li><li>testing</li></ul>")

  (test-end "inline-test")


  (html-renderer-test ((body (style ("test"))))
                      "<body><style>test</style></body>")

  (html-renderer-test (script)
                      "<script></script>")

  (test-end "rendering-test"))

(define (utils-tests)

  (define (incremental-match? target input)
    (let ((matcher (make-incremental-string-detector target)))
      (let loop ((i 0))
        (cond
          ((>= i (string-length input)) #f)
          ((matcher (string-ref input i)) i)
          (else (loop (+ i 1)))))))

  (test-begin "utils-test")

  (test-equal (incremental-match? "nice" "nice") 3)
  (test-equal (incremental-match? "nice" "hahanice") 7)

  (test-end "utils-test"))

(define (format-tests)
  (test-begin "format-test")
  (let-syntax ((format-test (syntax-rules ()
                              ((_ expr output-string)
                               (test-equal (with-output-to-string
                                             (lambda ()
                                               expr))
                                           output-string)))))
    (format-test (let ((a "nice"))
                   (nectar-format #t "@a"))
                 "nice"))
  (test-end "format-test"))

(define (converters-tests)
  (test-begin "converters-test")
  ;; Singular tag in markdown paragraph
  (test-equal "@p{@em{testing}}" (markdown-string->nectar-string "*testing*"))
  ;; Multiple items in markdown paragraph
  (test-equal "@em{testing} this!\n\nanother" (markdown-string->nectar-string "*testing* this!\n\nanother"))
  (test-end "converters-test"))



(define (main)
  (utils-tests)
  (parser-tests)
  (evaluation-tests)
  (renderer-tests)
  (format-tests)
  (converters-tests))

(main)
